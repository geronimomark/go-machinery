package main

import (
	"flag"
	"fmt"
    "log"

	machinery "github.com/RichardKnop/machinery/v1"
	"github.com/RichardKnop/machinery/v1/config"
	"github.com/RichardKnop/machinery/v1/errors"
	"github.com/RichardKnop/machinery/v1/signatures"

    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/postgres"
)

// Define flagss
var (
	configPath    = flag.String("c", "config.yml", "Path to a configuration file")
	broker        = flag.String("b", "redis://172.17.0.3:6379", "Broker URL")
	resultBackend = flag.String("r", "memcache://127.0.0.1:11211", "Result backend")
	exchange      = flag.String("e", "machinery_exchange", "Durable, non-auto-deleted AMQP exchange name")
	exchangeType  = flag.String("t", "direct", "Exchange type - direct|fanout|topic|x-custom")
	defaultQueue  = flag.String("q", "machinery_tasks", "Ephemeral AMQP queue name")
	bindingKey    = flag.String("k", "machinery_task", "AMQP binding key")

	cnf                                             config.Config
	server                                          *machinery.Server
	task0                                           signatures.TaskSignature
)

type Lg_jobs struct {
    JobId    int     `gorm:"column:lg_job_id"`
    Status   string
}

func init() {
	// Parse the flags
	flag.Parse()

	cnf = config.Config{
		Broker:        *broker,
		ResultBackend: *resultBackend,
		Exchange:      *exchange,
		ExchangeType:  *exchangeType,
		DefaultQueue:  *defaultQueue,
		BindingKey:    *bindingKey,
	}

	// Parse the config
	// NOTE: If a config file is present, it has priority over flags
	data, err := config.ReadFromFile(*configPath)
	if err == nil {
		err = config.ParseYAMLConfig(&data, &cnf)
		errors.Fail(err, "Could not parse config file")
	}

	server, err = machinery.NewServer(&cnf)
	errors.Fail(err, "Could not initialize server")
}

func initTasks(id int) {
	task0 = signatures.TaskSignature{
		Name: "dispatch",
		Args: []signatures.TaskArg{
			{
				Type:  "int",
				Value: id,
			},
		},
	}
}

func main() {

     log.Println("Client start...")

     log.Println("Connecting to postgres DB...")
     db, err := gorm.Open("postgres", "host=172.17.0.2 user=postgres dbname=testgo sslmode=disable password=mysecretpassword")
     if err != nil {
         log.Fatalln(err)
         return
     }

     log.Println("Get Queued jobs")
     var lgjobs []Lg_jobs

     db.Where("status = ?", "queued").Find(&lgjobs)
     for x := range lgjobs {
        log.Println("Sending...", lgjobs[x].JobId)

        initTasks(lgjobs[x].JobId)

        asyncResult, err := server.SendTask(&task0)
        errors.Fail(err, "Could not send task")

        result, err := asyncResult.Get()
        errors.Fail(err, "Getting task state failed with error")
        fmt.Printf("Dispathed ID: %v\n", result.Interface())
     }

}
