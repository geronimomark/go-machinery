package exampletasks

import (
    "log"

    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/postgres"
)

// type Lg_jobs struct {
//     JobId    int     `gorm:"column:lg_job_id"`
//     Status   string
// }

// Dispatch ...
func Dispatch(args ...int) (int, error) {
	id := int(0)
	for _, arg := range args {

        log.Println("Connecting to postgres DB...")
        db, err := gorm.Open("postgres", "host=172.17.0.2 user=postgres dbname=testgo sslmode=disable password=mysecretpassword")
        if err != nil {
            log.Fatalln(err)
            return id, nil
        }
        defer db.Close()

        // var lgjobs Lg_jobs
        id = arg
        // lgjobs.JobId = arg
        // lgjobs.Status = "queued"
        // db.Model(lgjobs).Update("Status", "dispatched")

        db.Exec("UPDATE lg_jobs SET status=? WHERE lg_job_id IN (?)", "dispatched", arg)
	}
	return id, nil
}
